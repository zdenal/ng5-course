import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { ErrorsComponent } from './errors/errors.component';
import { IsInvalidDirective } from './is-invalid.directive';
import { NotBlankDirective } from './not-blank.directive';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule
  ],
  declarations: [ErrorsComponent, IsInvalidDirective, NotBlankDirective],
  exports: [ErrorsComponent, IsInvalidDirective, NotBlankDirective]
})
export class ValidationModule { }
