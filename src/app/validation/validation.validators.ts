import { AbstractControl, ValidationErrors } from '@angular/forms';

export function notBlankValidator(control: AbstractControl): ValidationErrors {
  const value = control.value;
  return value === null || `${value}`.trim().length === 0 ? { notBlank: true } : null;
}
