import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UniversalValidators } from 'ngx-validators';

import { trimToNull } from '../../validation/validation.utils';
import { notBlankValidator } from '../../validation/validation.validators';
import { Car } from './../cars.model';
import { CarsService } from './../cars.service';

@Component({
  selector: 'app-cars-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ReactiveFormComponent implements OnInit {

  i18NPrefix = 'cars.form.';
  detailI18NPrefix = 'cars.detail.';

  @Input() car: Car;
  @Output() close = new EventEmitter<any>();
  @Output() save = new EventEmitter<Car>();

  alreadySubmitted = false;
  submitting = false;
  submitError = false;
  form: FormGroup;

  private isCreate: boolean;

  constructor(
    private carsService: CarsService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.isCreate = !this.car;
    if (!this.car) {
      this.car = {
        id: null,
        brand: null,
        model: null,
        fuel: 'diesel',
        power: null,
        description: null
      };
    }
    this.buildForm();
  }

  onClose(): void {
    this.close.emit(null);
  }

  onSubmit(form: FormGroup): void {
    this.alreadySubmitted = true;
    if (!form.valid || this.submitting) {
      return;
    }
    this.submitting = true;
    this.submitError = false;

    const value: Car = this.form.value;
    value.id = this.car.id;
    value.brand = trimToNull(value.brand);
    value.model = trimToNull(value.model);
    value.power = parseInt(trimToNull(value.power as any), 10);
    value.description = trimToNull(value.description);

    const obs = this.isCreate ? this.carsService.createCar$(value) : this.carsService.updateCar$(value);
    obs.subscribe((car) => {
      this.submitting = false;
      this.cd.markForCheck();
      this.save.emit(car);
    }, (e) => {
      console.error(e);
      this.submitError = true;
      this.submitting = false;
      this.cd.markForCheck();
    });
  }

  private buildForm(): void {
    this.form = this.fb.group({
      brand: [this.car.brand, notBlankValidator],
      model: [this.car.model, notBlankValidator],
      fuel: [this.car.fuel, notBlankValidator],
      power: [this.car.power, [notBlankValidator, UniversalValidators.isInRange(1, 1000)]],
      description: [this.car.description]
    });
  }

}
