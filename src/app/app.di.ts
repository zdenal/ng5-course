import { InjectionToken } from '@angular/core';

import { AppConfig } from './app.model';

export const appConfigToken = new InjectionToken<AppConfig>('app.config');
